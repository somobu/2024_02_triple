@tool
extends CSGBox3D



func _ready():
	material = material.duplicate(false)
	if randf() > 0.4:
		material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
		material.albedo_color = Color.GOLDENROD
	else:
		material.shading_mode = BaseMaterial3D.SHADING_MODE_PER_PIXEL
		material.albedo_color = Color.DIM_GRAY


