extends Node3D

var char = preload("res://playchar/player.tscn")

var char_instance = null

func _ready():
	get_tree().debug_collisions_hint = false
	goto_wild()



func goto_wild():
	char_instance = char.instantiate()
	add_child(char_instance)
	$MainCam.observed = char_instance.camHead
	
	char_instance.is_any_input = true
