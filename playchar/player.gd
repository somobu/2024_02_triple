extends CharacterBody3D


@export var move_speed = 2.0  # Move speed, units/s


const cam_sense_base = 0.003
@onready var camBase = $CamBase
@onready var camHead = $CamBase/Camera3D

var is_any_input = false
var is_movement_input = true


func _ready():
	$Control/CrosshairTooltip.text = ""
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _input(event):
	if event is InputEventMouseMotion:
		camBase.rotation.y -= event.relative.x * cam_sense_base
		camHead.rotation.x -= event.relative.y * cam_sense_base


func _physics_process(delta):
	
	# Movement
	if is_movement_input and is_on_floor():
		var input_vec = Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
		var move_vec = input_vec.rotated(-camBase.global_rotation.y).normalized()
		var move_glob = Vector3(move_vec.x, 0, move_vec.y) * move_speed
		velocity = velocity.lerp(move_glob, 0.1)
		
		var input_h_vec = Input.get_action_strength("move_jump") * 8.0
		if input_h_vec > 0: velocity.y = input_h_vec
	
	velocity.y -= 30.0 * delta
	
	move_and_slide()
